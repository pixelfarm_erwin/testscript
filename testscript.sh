#!/bin/bash
echo -n "Project name: "
read project

echo -n "Parent folder (started at ~/Sites/) leave empty for directly in ~/Sites/: "
read parent

echo -n "Database name2: "
read database

if [ -z ${MACHINE+x} ]; then echo -n "Your machine name (minotaur, falcon etc): " && read MACHINE; else echo "Your machine name is '$MACHINE'"; fi
if [ -z ${BITBUCKUSER+x} ]; then echo -n "Bitbucket user: " && read BITBUCKUSER; else echo "Your Bitbucket user: $BITBUCKUSER"; fi
if [ -z ${BITBUCKPW+x} ]; then echo -n "Bitbucket password: " && read BITBUCKPW; else echo "Your Bitbucket password is set"; fi
if [ -z ${BITBUCKTEAM+x} ]; then echo -n "Bitbucket team: " && read BITBUCKTEAM; else echo "Your Bitbucket team: $BITBUCKTEAM"; fi
if [ -z "${parent// }" ]; then sitepath=/Users/$USER/Sites/; else sitepath=/Users/$USER/Sites/$parent; fi

cd $sitepath
echo -n "Pulling the empty wordpress git bucket" &&
git clone git@bitbucket.org:$BITBUCKTEAM/empty-wordpress.git $project &&
cd $project/ &&
git fetch --all && 
git pull --all &&
git checkout master &&
cd newdb/ &&
echo -n "Creating the database if it doesnt exist yet" &&
DBEXISTS=$(mysql --batch --skip-column-names -u dust -h dust.pxlfrm.com -e "SHOW DATABASES LIKE '"$database"';" | grep "$database" > /dev/null; echo "$?")
if [ $DBEXISTS -eq 0 ]; then
	echo "A database with the name $db already exists. exiting"
else
	mysql -u dust -h dust.pxlfrm.com -e 'create database if not exists '$database'';
	mysql -u dust -h dust.pxlfrm.com $database < newdatabase.sql;
fi
cd ../public_html &&
echo "Adding records to the virtualhost.httpd" &&
ex -sc '%s/erwintestdb/'$database'/g|x' wp-config.local.php &&
echo '
###### '$project' 
<VirtualHost *:8080>
  ServerName '$project'.'$MACHINE'.pxlfrm.com
  CustomLog "/Users/erwin/Sites/logs/project.dev-access_log" combined
  ErrorLog "/Users/erwin/Sites/logs/project.dev-error_log"
  DocumentRoot "'$sitepath'/'$project'/public_html/"
</VirtualHost> ' >> /Users/$USER/Sites/httpd-vhosts.conf &&
echo -n "Restarting apache" &&
brew services restart httpd22 &&
echo -n "Inserting repo in: $BITBUCKTEAM" &&
curl -X POST -v -u $BITBUCKUSER:$BITBUCKPW "https://api.bitbucket.org/2.0/repositories/$BITBUCKTEAM/"$project"" -d '{"scm": "git", "is_private": "true", "fork_policy": "no_public_forks"}'